#!/bin/sh

if [ "$CI_COMMIT_TAG" ] ; then
  . version
  EXTRA_VERSION=2
  if [ "$EXTRA_VERSION" ] ; then
    VERSION=$VERSION-$EXTRA_VERSION
  fi
  IMAGE_NAME=aragaer/xtomp:$VERSION
else
  IMAGE_NAME=aragaer/xtomp
fi

docker login -u $CI_GITLAB_REG_USER -p $CI_GITLAB_REG_PASS $CI_REGISTRY

if docker pull $CI_REGISTRY/aragaer/xtomp/builder ; then
    docker build -t $IMAGE_NAME -f container/Containerfile.from_builder --build-arg CI_REGISTRY=$CI_REGISTRY .
else
    docker build -t $IMAGE_NAME -f container/Containerfile .
fi

