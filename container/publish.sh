#!/bin/sh
if [ -z "$DOCKER_USER" ] ; then
    echo "Not publishing"
    exit 0
fi

docker login -u $DOCKER_USER -p $DOCKER_PASS docker.io

if [ "$CI_COMMIT_TAG" ] ; then
  . version
  EXTRA_VERSION=2
  if [ "$EXTRA_VERSION" ] ; then
    VERSION=$VERSION-$EXTRA_VERSION
  fi
  IMAGE_NAME=aragaer/xtomp:$VERSION
else
  IMAGE_NAME=aragaer/xtomp
fi

docker push docker.io/$IMAGE_NAME
